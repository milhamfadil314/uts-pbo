import java.util.Scanner;
public class UTS021200111 {

            public static void main(String[] args) {
                
                String npm="";
                String nama="";
                String matkul="";
                double nilai_akhir=0;
                int input_nilai_tugas;
                int input_nilai_uts;
                int input_nilai_uas;
                String grade = " ";

                Scanner n=new Scanner(System.in);
                System.out.print("Npm : ");
                npm=n.nextLine();
                System.out.print("Nama Mahasiswa : ");
                nama=n.nextLine();
                System.out.print("Nama Matakuliah : ");
                matkul=n.nextLine();
                System.out.print("Masukan Nilai tugas : ");
                input_nilai_tugas=n.nextInt();
                System.out.print("Masukan Nilai uts : ");
                input_nilai_uts=n.nextInt();
                System.out.print("Masukan Nilai uas : ");
                input_nilai_uas=n.nextInt();

                nilai_akhir = (input_nilai_tugas*40/100) + (input_nilai_uas*35/100) + (input_nilai_uts*25/100);

                if(nilai_akhir >=85 && nilai_akhir <=100)
                {
                    grade="A";
                }
                else if(nilai_akhir>=70 && nilai_akhir<=85)
                {
                    grade="B";
                }
                else if (nilai_akhir>=60 && nilai_akhir<=70)
                {
                    grade="C";
                }
                else if(nilai_akhir <=50 && nilai_akhir >=60)
                {
                    grade="D";
                }
                else if(nilai_akhir <=0 && nilai_akhir >=50)
                {
                    grade="E";
                }

                System.out.println("____________________________");
                System.out.println("Npm : "+npm+"\n"+"Nama Mahasiswa : "+nama+"\n"+"Nama Matakuliah : "+matkul+"\n"+"nilai : "+nilai_akhir+"\n"+"Grade : "+ grade);
            }
}